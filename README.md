# Git Pipeline

This type of repository allows deploying different custom configuration using script from simloud-pipeline file.

For more information, please, use documentation <a href="https://docs.simloud.com/en/how-to-use-simloud-files/" target="_blank">How to use Simloud files</a>.
